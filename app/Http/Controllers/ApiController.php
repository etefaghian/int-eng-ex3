<?php

namespace App\Http\Controllers;

use App\Person;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function getPersons() {
        $Person = Person::get()->toJson(JSON_PRETTY_PRINT);
        return response($Person, 200);
    }

    public function createPerson(Request $request)
    {
        $person = new Person;
        $person->full_name = $request->full_name;
        $person->nation_code = $request->nation_code;
        $person->student_id = $request->student_id ;
        $person->age = $request->age;
        $person->phone = $request->phone;
        $person->address = $request->address;
        $person->save();

        return response()->json([
            "message" => "person record created"
        ], 201);

    }


    public function getPerson($nationId)
    {
        if (Person::where('nation_code', $nationId)->exists()) {
            $Person = Person::where('nation_code', $nationId)->get()->toJson(JSON_PRETTY_PRINT);
            return response($Person, 200);
        } else {
            return response()->json([
                "message" => "Person not found"
            ], 404);
        }
    }

}
