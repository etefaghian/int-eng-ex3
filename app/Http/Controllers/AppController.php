<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class AppController extends Controller
{
    public function showInformation(Request $request)
    {
        //step 1 get information form request
        $nationCode=$request->nationCode;
        //step 2 get information from api
        $req = Request::create('http://localhost:8000/api/persons/'.$nationCode, 'GET');
        $res = app()->handle($req);
        $responseBody = $res->getContent();
        //step 3 convert json to object
        $persons =json_decode($responseBody);
        //step 4 return view with model
        return view('show', ['persons' => $persons]);
    }
}
