<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $table = "person";
    protected $fillable = ['full_name', 'nation_code','student_id','age','phone','address'];

}
