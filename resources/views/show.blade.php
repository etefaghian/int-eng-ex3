<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link
        rel="stylesheet"
        href="https://cdn.rtlcss.com/bootstrap/v4.2.1/css/bootstrap.min.css"
        integrity="sha384-vus3nQHTD+5mpDiZ4rkEPlnkcyTP+49BhJ4wJeJunw06ZAp+wzzeBPUXr42fi8If"
        crossorigin="anonymous">    <title>Document</title>
</head>
<body>

<div class="container">
    <table class="table table-striped">
        <thead class="table-warning">
        <tr>
            <th scope="col">شناسه کاربری</th>
            <th scope="col">نام کامل</th>
            <th scope="col">کد ملی</th>
            <th scope="col">شماره دانشجویی</th>
            <th scope="col">سن</th>
            <th scope="col">شماره تلفن</th>
            <th scope="col">آدرس</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($persons as $person)
            <tr class="table-success">
                <th scope="row">{{$person->id}}</th>
                <td>{{$person->full_name}}</td>
                <td>{{$person->nation_code}}</td>
                <td>{{$person->student_id}}</td>
                <td>{{$person->age}}</td>
                <td>{{$person->phone}}</td>
                <td>{{$person->address}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>













<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.rtlcss.com/bootstrap/v4.2.1/js/bootstrap.min.js" integrity="sha384-a9xOd0rz8w0J8zqj1qJic7GPFfyMfoiuDjC9rqXlVOcGO/dmRqzMn34gZYDTel8k" crossorigin="anonymous"></script>

<script type="text/javascript">


</script>

</body>
</html>
