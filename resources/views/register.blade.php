<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link
        rel="stylesheet"
        href="https://cdn.rtlcss.com/bootstrap/v4.2.1/css/bootstrap.min.css"
        integrity="sha384-vus3nQHTD+5mpDiZ4rkEPlnkcyTP+49BhJ4wJeJunw06ZAp+wzzeBPUXr42fi8If"
        crossorigin="anonymous">    <title>Document</title>
</head>
<body >

<div class="modal fade " id="exampleModal"  role="dialog" >
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header ">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <h5 class="modal-title" id="exampleModalLabel">ثبت نام جدید</h5>


            </div>
            <div class="modal-body">
                <form method="post" action="./api/persons" id="my-form" >
                    <div class="form-group  justify-content-center align-items-center">
                        <label for="full_name" class="col-form-label">نام کامل</label>
                        <input type="text" class="form-control" id="fullName" name="full_name">
                    </div>
                    <div class="form-group  justify-content-center align-items-center">
                        <label for="nation-code" class="col-form-label">کدملی:</label>
                        <input type="text" class="form-control" id="nation-code" name="nation_code">
                    </div>
                    <div class="form-group  justify-content-center align-items-center">
                        <label for="student_id" class="col-form-label">شماره دانشجویی:</label>
                        <input type="text" class="form-control" id="student_id" name="student_id">
                    </div>
                    <div class="form-group  justify-content-center align-items-center">
                        <label for="age" class="col-form-label">سن:</label>
                        <input type="text" class="form-control" id="age" name="age">
                    </div>
                    <div class="form-group  justify-content-center align-items-center">
                        <label for="phone" class="col-form-label">شماره تلفن:</label>
                        <input type="text" class="form-control" id="phone" name="phone">
                    </div>
                    <div class="form-group  justify-content-center align-items-center">
                        <label for="address" class="col-form-label">آدرس</label>
                        <input type="text" class="form-control" id="address" name="address">
                    </div>
                </form>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
                <button id="my-submit" type="submit" class="btn btn-primary btn-lg" style="width: 30%;">ارسال </button>
            </div>
        </div>
    </div>
</div>






<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.rtlcss.com/bootstrap/v4.2.1/js/bootstrap.min.js" integrity="sha384-a9xOd0rz8w0J8zqj1qJic7GPFfyMfoiuDjC9rqXlVOcGO/dmRqzMn34gZYDTel8k" crossorigin="anonymous"></script>

<script type="text/javascript">
    $(window).on('load',function(){
        $('#exampleModal').modal('show');
    });
    document.getElementById('my-submit').addEventListener('click',function () {
        document.getElementById('my-form').submit();
    })


</script>

</body>
</html>
