<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});


//for show register form
Route::get('/register',function (){
    return view('register');
});

//for signup
Route::get('/signup','AppController@signup');





//for show information
Route::post('/showInformation','AppController@showInformation');



